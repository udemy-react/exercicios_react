import React from 'react'

function childrenWithProps(children, props) {
    return React.Children.map(
        children, 
        childAtual => React.cloneElement(childAtual, props)           
    )
}

export { childrenWithProps }