const InitialState = { value: 'opa' }

export default function (state = InitialState, action) {
    switch (action.type) {
        case "VALUE_CHANGED":
            return { value: action.payload }

        default:
            return state
    }
}