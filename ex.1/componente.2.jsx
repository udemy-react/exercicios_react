import React from 'react'

export default (props) => (
    <h3>First component</h3>
)

export const SecondComponent = (props) => (
    <h3>Second component</h3>
)

//export { FirstComponent, SecondComponent }