import React from 'react'
import ReactDOM from 'react-dom'
import NovoComponente from './componente'
import NovoComponente1 from './componente.1'
import FirstComponent, {SecondComponent} from './componente.2'

ReactDOM.render(
    <div>
        <h1>Ola mundo</h1>
        <NovoComponente />
        <NovoComponente1 pValue="Show" />
        <FirstComponent />
        <SecondComponent />
    </div>
    , document.getElementById('app'))
