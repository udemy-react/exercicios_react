import React from 'react'
import ReactDOM from 'react-dom'
import ComponentePai from './componentePai'
import ComponenteFilho from './componenteFilho'

ReactDOM.render(
    <div>
        <ComponentePai lastName='Silva'>
            <ComponenteFilho name='João' />
            <ComponenteFilho name='Carlos' />
        </ComponentePai>

        <ComponentePai lastName='Coutinho'>
            <ComponenteFilho name='Felipe' />
            <ComponenteFilho name='Monica' />
        </ComponentePai>
        </div>
    , document.getElementById('app'))
