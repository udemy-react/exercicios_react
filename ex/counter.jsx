import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { inc, dec, stepChanged } from './counterActions'

const Counter = (props) => (
    <div>
        <h4>{props.counter.number}</h4>
        <button onClick={props.dec}>-</button>
        <input type="text" onChange={props.stepChanged} value={props.counter.step} />
        <button onClick={props.inc}>+</button>
    </div>
)

const mapStateToProps = (state) => ({counter: state.counter})
const mapDispatchToProps = dispach => bindActionCreators({inc, dec, stepChanged}, dispach)

export default connect(mapStateToProps, mapDispatchToProps)(Counter)