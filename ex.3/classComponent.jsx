import React from 'react'

export default class ClassComponent extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = { value: props.initialValue }
    }

    soma(pDelta) {
        this.setState({ value: this.state.value + pDelta })
    }

    render() {
        return (
            <div>
                <h1>{this.props.label}</h1>
                <h2>{this.props.initialValue}</h2>
                <h3>{this.state.value}</h3>
                <button onClick={ () => this.soma(-1) }> - </button>
                <button onClick={ () => this.soma(1) }> + </button>
            </div>
        )
    }
}